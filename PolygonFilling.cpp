


#include "PolygonFilling.h"
#include "MyLinkedList.hpp"




using namespace std;

float zbuffer[WINDOW_HEIGHT+1][WINDOW_WIDTH+1];



void initZbuffer(void)
{
    for (int i = 0; i < WINDOW_WIDTH; i++) {
        for (int j = 0; j < WINDOW_HEIGHT; j++) {
            zbuffer[i][j] = 0;
        }
    }
}

bool checkZbuffer(int x, int y, float z)
{
    if(zbuffer[x][y] <= z)
    {
        zbuffer[x][y] = z;
        return true;
    }
    return false;
}




void PolygonFilling::drawPointMarker()
{
	glPointSize(3.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin(GL_POINTS);
	
	for (Hcoords p : polygonPoints)
	{
		glVertex2i(p.x, p.y);
	}

	glEnd();
}


void PolygonFilling::drawLinePolygon()
{

    glColor3f(0, 0, 0);
    for(int i = 0; i < polygonPoints.size()/3  ; i++)
    {
        glBegin(GL_LINES);
        glVertex2i(polygonPoints[i].x       , polygonPoints[i].y);
        glVertex2i(polygonPoints[i+1].x     , polygonPoints[i+1].y);
        
        glVertex2i(polygonPoints[i+1].x     , polygonPoints[i+1].y);
        glVertex2i(polygonPoints[i + 2].x   , polygonPoints[i + 2].y);
        
        glVertex2i(polygonPoints[i+2].x     , polygonPoints[i+2].y);
        glVertex2i(polygonPoints[i].x       , polygonPoints[i].y);
        glEnd();
    }
}


void PolygonFilling::clear()
{
	isDrawingLinePolygon = false;
	isFillingColor = false;
	isDrawingPointMarker = true;

	polygonPoints.clear();

	glutPostRedisplay();
}


void printETs(ET_LinkedList **et)
{
	for (int i = 0; i < MAX_HEIGHT_OF_SCANLINE; i++)
	{
		if (et[i] != NULL)
		{
			cout << "[" << i << "]";
			et[i]->print();
		}
	}
	
}


void PolygonFilling::drawPointsWithRange(int start, int end, int y, float zLeft, float zRight, float intensityLeft, float intensityRight, Hcoords colorLeft, Hcoords colorRight)
{
	if (y < 0 || y > WINDOW_HEIGHT)
		return;

    
    glPointSize(1.0f);
    
    //cout << intensityLeft <<  "  "  << intensityRight << endl;

    int difX = end - start;
    
    float difI = intensityRight - intensityLeft;
    float difIbydifX = difI / difX;
    float intensity = intensityLeft;
    
    
    float difZ = zRight - zLeft;
    float difZbyDifX = difZ / difX;
    float z = zLeft;
    
    
    Hcoords difColor = colorRight - colorLeft;
    Hcoords difColorByDifX = difColor / difX;
    Hcoords color = colorLeft;
    
    
    
    
	for (int i = start ; i <= end; i++)
	{

        
        if ( (i < 0 || i > WINDOW_WIDTH)  || !checkZbuffer(i, y, z))
        {
            intensity = intensity + difIbydifX;
            z = z + difZbyDifX;
            color = color + difColorByDifX;
            continue;
        }
        
        glColor3f(color.x * intensity, color.y * intensity, color.z * intensity);
        
        glBegin(GL_POINTS);
        glVertex2i(i, y);
        glEnd();
        
        intensity = intensity + difIbydifX;
        z = z + difZbyDifX;
        color = color + difColorByDifX;
	}
}


void PolygonFilling::setupET()
{
    for (int  i = 0; i < MAX_HEIGHT_OF_SCANLINE; i++)
    {
        ET[i] = NULL;
    }
    
    int smallestY;
    int yMax;
    float xMin;
    float invM;
    

    float zMax;
    float zMin;
    float inverseZ;
    
    Hcoords p1;
    Hcoords p2;
    
    float intensityMax;
    float intensityMin;
    float intensity1;
    float intensity2;
    float inverseIntensity;
    
    Hcoords color1;
    Hcoords color2;
    Hcoords colorMax;
    Hcoords colorMin;
    Hcoords inverseColor;
    
    for (int i = 0; i < polygonPoints.size(); i++)
    {
       
        
        if (i == polygonPoints.size() - 1)
        {
            p1 = polygonPoints[polygonPoints.size() - 1];
            p2 = polygonPoints[0];
            
            intensity1 = intensitys[polygonPoints.size() - 1];
            intensity2 = intensitys[0];
            
            color1 = polygonColors[polygonPoints.size() - 1];
            color2 = polygonColors[0];
            
        }
        else
        {
            p1 = polygonPoints[i];
            p2 = polygonPoints[i + 1];
            
            intensity1 = intensitys[i];
            intensity2 = intensitys[i+1];
            
            color1 = polygonColors[i];
            color2 = polygonColors[i+1];
        }
        
        
        if (p1.y >= p2.y)
        {
            smallestY = p2.y;
            yMax = p1.y;
            xMin = p2.x;
            
            zMin = p2.z;
            zMax = p1.z;
            
            intensityMax = intensity1;
            intensityMin = intensity2;
            
            colorMin = color2;
            colorMax = color1;
        }
        else
        {
            smallestY = p1.y;
            yMax = p2.y;
            xMin = p1.x;
            
            zMin = p1.z;
            zMax = p2.z;
            
            intensityMax = intensity2;
            intensityMin = intensity1;
            
            colorMin = color1;
            colorMax = color2;
        }
        
        if(p1.y == p2.y)
        {
            invM = 0;
            
            inverseIntensity = 0;
            
            inverseZ = 0;
            
            inverseColor = Hcoords(0, 0, 0, 1);
            
        }else
        {
            invM = (float)(p2.x - p1.x) / (p2.y - p1.y);
            
            inverseIntensity = (intensityMax - intensityMin) / (yMax - smallestY);
            
            inverseZ = (zMax - zMin) / (yMax - smallestY);
            
            inverseColor = (colorMax - colorMin) / (yMax - smallestY);
        }
        
        
        if (ET[smallestY] == NULL)
            ET[smallestY] = new ET_LinkedList();
        

         
        ET[smallestY]->add(yMax ,xMin ,invM ,zMin ,inverseZ ,intensityMin ,inverseIntensity, colorMin, inverseColor);
        
        //cout << smallestY << endl;
        counter++;
    }
    
    counterDefault = counter;
    

}

void PolygonFilling::fillColor()
{
    ET_LinkedList AET = ET_LinkedList();

    counter = counterDefault;
    
    for (int i = 0; i < HEIGHT;	i++)
	{
		int start;
		int end;
        float zLeft;
        float zRight;
        float intensityLeft;
        float intensityRight;
        Hcoords colorLeft;
        Hcoords colorRight;

		if (ET[i] != NULL)
		{
			for (int  j = 0; j < ET[i]->getLength(); j++)
			{
				ET_Node n = ET[i]->getNode(j);
				AET.add(n.maximumY, n.minimunX, n.inverseM, n.zMin, n.inverseZ, n.intensity, n.inverseIntensity, n.color, n.inverseColor);
                counter--;
			}
		}

		AET = AET.sort();
		
		AET.removeAtY(i);

		for (int  j = 0; j <= AET.getLength()/2 && AET.getLength() >= 2 ; j +=2 )
		{
			float endTemp = AET.getNode(j + 1).minimunX;
			start = round(AET.getNode(j).minimunX);
			end = floor(endTemp);
            
            intensityLeft = AET.getNode(j).intensity;
            intensityRight = AET.getNode(j+1).intensity;
            
            zLeft = AET.getNode(j).zMin;
            zRight = AET.getNode(j+1).zMin;
            
            colorLeft = AET.getNode(j).color;
            colorRight = AET.getNode(j+1).color;
            

			if (endTemp / end == 0)
			{
				end--;
			}


			drawPointsWithRange(start, end, i, zLeft, zRight, intensityLeft, intensityRight, colorLeft, colorRight);
			
		}
		
		AET.updateX();
        
//        cout << AET.getLength();
        if(AET.getLength() <= 0  && counter <= 0)
        {
            break;
        }
        //cout << i;
    }
   // printETs(ET);
	
}


void PolygonFilling::init()
{
    
    PolygonFilling::setupET();
    
    

	isDrawingLinePolygon = false;
	isFillingColor = true;
	isDrawingPointMarker = false;
}


PolygonFilling::PolygonFilling(vector<Hcoords> pps, vector<float> is, vector<Hcoords> pcs)
{
    
    for (int i = 0; i < pps.size(); i++)
    {
        polygonPoints.push_back(pps[i]);
        
    }
    
    for (int i = 0; i < pcs.size(); i++)
    {
        polygonColors.push_back(pcs[i]);
        
    }
    for (int i = 0; i < is.size(); i++)
    {
        intensitys.push_back(is[i]);
        
    }
    
    init();
    
    
    //cout << polygonPoints.size();
    //printETs(ET);
}





void PolygonFilling::display(void)
{

	if (isFillingColor)
	{
        fillColor();
	}

	if (isDrawingLinePolygon)
	{
		drawLinePolygon();
	}

	if (isDrawingPointMarker)
	{
		drawPointMarker();
	}


}





