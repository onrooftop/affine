//Panupong Kukutapan 5709680010
//Assignment 2
//Computer Graphics 2
//Semester 2/2016



#include "Projection.h"



vector<Hcoords> out_vertices;
vector<Hcoords> points;
Hcoords VRP,VPN,VUP,PRP,window;
float F, B, Zmin;

int type = -1;


ostream& operator<<(ostream& s, const Affine& A) {
    s << '{';
    for (int i=0; i < 4; ++i) {
        s << '{';
        for (int j=0; j < 4; ++j)
            s << A[i][j] << ((j < 3) ? ',' : '}');
        s << ((i < 3) ? ',' : '}');
        cout << "\n";
    }
    return s;
}


bool readOBJFile(const char* path, vector<Hcoords> &out_vertices)
{
	FILE* file = fopen(path, "r");

    
	if (file == NULL)
	{
		cout << "Can't open the file " << path <<"!\n";
		return false;
	}

	vector<Point> temp_vertices;
	vector<unsigned int> vertexIndices;

	while (true)
	{
		char lineHeader[128];

		int result = fscanf(file, "%s", lineHeader);

		if (result == EOF)
			break;

		if (strcmp(lineHeader, "v") == 0)
		{
			float x, y, z;
			fscanf(file, "%f %f %f\n", &x, &y, &z);
			temp_vertices.push_back(Hcoords(x, y, z,1));
		}

		if (strcmp(lineHeader, "f") == 0)
		{
			unsigned int v1, v2, v3;
			fscanf(file, "%d/%d/%d\n", &v1, &v2, &v3);
			vertexIndices.push_back(v1);
			vertexIndices.push_back(v2);
			vertexIndices.push_back(v3);
		}
		
		if (strcmp(lineHeader, "VRP") == 0)
		{
			float x, y, z;
			fscanf(file, "%f %f %f\n", &x, &y, &z);
			VRP = Hcoords(x, y, z, 0);
		}
		if (strcmp(lineHeader, "VPN") == 0)
		{
			float x, y, z;
			fscanf(file, "%f %f %f\n", &x, &y, &z);
			VPN = Hcoords(x, y, z, 0);
		}
		if (strcmp(lineHeader, "VUP") == 0)
		{
			float x, y, z;
			fscanf(file, "%f %f %f\n", &x, &y, &z);
			VUP = Hcoords(x, y, z, 0);
		}
		if (strcmp(lineHeader, "PRP") == 0)
		{
			float x, y, z;
			fscanf(file, "%f %f %f\n", &x, &y, &z);
			PRP = Hcoords(x, y, z, 1);
		}
		if (strcmp(lineHeader, "WINDOW") == 0)
		{
			float x, y, z, w;
			fscanf(file, "%f %f %f %f\n", &x, &y, &z, &w);
			window = Hcoords(x, y, z, w);
		}

		if (strcmp(lineHeader, "F") == 0)
		{
			fscanf(file, "%f\n", &F);
		}

		if (strcmp(lineHeader, "B") == 0)
		{
			fscanf(file, "%f\n", &B);
		}
	}

    
    
	for (int i = 0; i < vertexIndices.size()/*temp_vertices.size()*/; i++)
	{
		out_vertices.push_back(temp_vertices[vertexIndices[i]]);
		//out_vertices.push_back(temp_vertices[i]);
	}
    
    return true;
}


void parProjection()
{
	if (!readOBJFile("/Users/panupong/Desktop/openGLProject/openGLProject/Parallel.txt", out_vertices))
		return;

	points.clear();

	Affine TVRP = Trans(-VRP);
	Hcoords u, v;
	u = cross(VUP, VPN);
	v = cross(VPN, u);

	u = Normalize(u);
	v = Normalize(v);

	Point dsp;
	Affine r = CamToWorld(u, v, VPN);

	Hcoords CW((window.x + window.y) / 2, (window.z + window.w) / 2, 0, 1);

	Hcoords DOP = CW - PRP;

	DOP = Normalize(DOP);

	Affine SHpar = Shear(-(DOP.x / DOP.z), -(DOP.y / DOP.z));

	Affine Tpar = Trans(Vector((window.x + window.y) / -2, (window.z + window.w) / -2, -F));

	Affine Spar = Scale(2 / (window.y - window.x), 2 / (window.w - window.z), 1 / (F - B));

	Affine Mort = Scale(1, 1, 0);

	Affine Npar = /*Mort * */Spar * Tpar * SHpar * r * TVRP;

	Affine T111 = Trans(Vector(1, 1, 1));

	Affine ScaleIntoSizeOf3DVP = Scale((vpXmax - vpXmin) / 2,
		(vpYmax - vpYmin) / 2,
		(vpZmax - vpZmin) / 1);

	Affine TransToLowerLeftConerOf3DVP = Trans(Vector(vpXmin, vpYmin, vpZmin));

	Affine Mvv3dv = TransToLowerLeftConerOf3DVP * ScaleIntoSizeOf3DVP * T111;

	for (int i = 0; i < out_vertices.size(); i++)
	{
		points.push_back( DivideByW(Mvv3dv * Npar * out_vertices[i]));
	}
	
}


void perProjection()
{
	if (!readOBJFile("/Users/panupong/Desktop/openGLProject/openGLProject/Perspective.txt",out_vertices) )
		return;

	points.clear();

	Affine TVRP = Trans(-VRP);
	Hcoords u, v;
	u = cross(VUP, VPN);
	v = cross(VPN, u);

	u = Normalize(u);
	v = Normalize(v);

	Point dsp;
	Affine r = CamToWorld(u, v, VPN);

	Vector minusPRP = -Vector(PRP.x, PRP.y, PRP.z);

	Affine TPRP = Trans(minusPRP);

	Hcoords CW((window.x + window.y) / 2, (window.z + window.w) / 2, 0, 1);

	Hcoords DOP = CW - PRP;

	Affine SHper = Shear(-(DOP.x / DOP.z), -(DOP.y / DOP.z));

	Affine Sper = Scale((2 * minusPRP.z) / ((window.y - window.x) * (minusPRP.z + B)),
		(2 * minusPRP.z) / ((window.w - window.z)*(minusPRP.z + B)),
		-1 / (minusPRP.z + B));


	Affine Nper = Sper * SHper * TPRP * r * TVRP;

	Zmin = -(minusPRP.z + F) / (minusPRP.z + B);

	Affine M = MappingIntoViewport(Zmin);

	Affine Mort = Scale(1, 1, 0);

	Affine T111 = Trans(Vector(1, 1, 1));

	Affine ScaleIntoSizeOf3DVP = Scale( (vpXmax - vpXmin) / 2,
		(vpYmax - vpYmin) / 2,
		(vpZmax - vpZmin) / 1 );

	Affine TransToLowerLeftConerOf3DVP = Trans(Vector(vpXmin, vpYmin, vpZmin));

	Affine Mvv3dv = TransToLowerLeftConerOf3DVP * ScaleIntoSizeOf3DVP * T111;

    //cout << Mvv3dv << M << Nper;
    
    Affine f = Mvv3dv * /*Mort * */ M * Nper;
 
    //cout << f;
	for (int i = 0; i < out_vertices.size(); i++)
	{
        
        Hcoords temp = DivideByW(Mvv3dv * /*Mort * */ M * Nper * out_vertices[i]);
		points.push_back(temp);
        
	}
	

}



void initProjection(int const typeOfProjection)
{
    gluOrtho2D(vpXmin, vpXmax, vpYmin, vpYmax);
    type = typeOfProjection;
}


vector<Hcoords> project(vector<Hcoords> &vertices)
{
    out_vertices.clear();
    out_vertices = vertices;
    
    
    if( type == PERSPECTIVE)
    {
        perProjection();
        
    }else{
        parProjection();
    }
    
    return points;
}


vector<Hcoords> project(char const *path)
{
    out_vertices.clear();
    readOBJFile(path, out_vertices);
    
    if( type == PERSPECTIVE)
    {
        perProjection();
        
    }else{
        parProjection();
    }
    
    return points;
}

bool readOBJ(const char* path, vector<Hcoords> &out_vertices, vector<Hcoords> &normals, vector<Hcoords> &colors)
{
    FILE* file = fopen(path, "r");
    
    
    if (file == NULL)
    {
        cout << "Can't open the file " << path <<"!\n";
        return false;
    }
    
    vector<Point> temp_vertices;
    vector<Point> temp_colors;
    vector<unsigned int> vertexIndices;
    
    while (true)
    {
        char lineHeader[128];
        
        int result = fscanf(file, "%s", lineHeader);
        
        if (result == EOF)
            break;
        
        if (strcmp(lineHeader, "v") == 0)
        {
            float x, y, z;
            fscanf(file, "%f %f %f\n", &x, &y, &z);
            temp_vertices.push_back(Hcoords(x, y, z,1));
        }
        
        if (strcmp(lineHeader, "f") == 0)
        {
            unsigned int v1, v2, v3;
            fscanf(file, "%d/%d/%d\n", &v1, &v2, &v3);
            vertexIndices.push_back(v1);
            vertexIndices.push_back(v2);
            vertexIndices.push_back(v3);
        }
        
        if (strcmp(lineHeader, "c") == 0)
        {
            float c1, c2, c3;
            Hcoords c;
            fscanf(file, "%f %f %f\n", &c1, &c2, &c3);
            c = Hcoords(c1, c2, c3, 1);
            temp_colors.push_back(c);

        }
        
    }
    
    
    for (int i = 0; i < vertexIndices.size()/*temp_vertices.size()*/; i++)
    {
        out_vertices.push_back(temp_vertices[vertexIndices[i]]);
        //out_vertices.push_back(temp_vertices[i]);
        //cout << i << endl;
    }
    
    
    for (int i = 0; i < vertexIndices.size(); i++)
    {
        colors.push_back(temp_colors[vertexIndices[i]]);

    }
    
    for (int i = 0; i <= vertexIndices.size() - 3; i += 3)
    {
        Hcoords a = temp_vertices[vertexIndices[i]];
        Hcoords b = temp_vertices[vertexIndices[i+1]];
        Hcoords c = temp_vertices[vertexIndices[i+2]];
        
        //cout << "index " << vertexIndices.size() << " "<< c.w << endl;
        
        Vector normal = cross(b-a, c-a);

        normal = Normalize(normal);
        
        normals.push_back(normal);
        normals.push_back(normal);
        normals.push_back(normal);
    }
    
    
    
    return true;
}

