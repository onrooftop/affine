//
//  MyLinkedList.cpp
//  openGLProject
//
//  Created by Panupong Kukutapan on 4/18/2560 BE.
//  Copyright © 2560 Panupong Kukutapan. All rights reserved.
//

#include "MyLinkedList.hpp"

ostream& operator<<(ostream& s, const Hcoords& v) {
    return s << '[' << v.x << ',' << v.y << ',' << v.z << "," << v.w << ']';
}


ET_LinkedList::ET_LinkedList(void)							//This is a constructure using to set private variables.
{
    head = NULL;
    length = 0;
    
}

void ET_LinkedList::add(int yMax, float xMin, float invM, float zmin, float inversez, float I, float inverseI, Hcoords c, Hcoords inverseC )	//Add fnc, for adding a new node into the database.
{
    ET_Node *temp = new ET_Node;
    temp->maximumY = yMax;
    temp->minimunX = xMin;
    temp->inverseM = invM;
    
    temp->intensity = I;
    temp->inverseIntensity = inverseI;
    
    temp->zMin = zmin;
    temp->inverseZ = inversez;
    
    temp->color = c;
    temp->inverseColor = inverseC;
    
    temp->nextNode = NULL;
    
    if (head == NULL)
    {
        head = temp;
        length++;
        
    }
    else
    {
        //sort node with minimum of x.
        ET_Node *runLoopNode = head;
        if (runLoopNode->minimunX > temp->minimunX)
        {
            temp->nextNode = runLoopNode;
            head = temp;
            length++;
            return;
        }
        
        while (runLoopNode != NULL && runLoopNode->nextNode != NULL)
        {
            if (runLoopNode->nextNode->minimunX > temp->minimunX)
            {
                temp->nextNode = runLoopNode->nextNode;
                runLoopNode->nextNode = temp;
                length++;
                return;
            }
            
            runLoopNode = runLoopNode->nextNode;
        }
        
        runLoopNode->nextNode = temp;
        length++;
        return;
        
    }
}

void ET_LinkedList::print()
{
    ET_Node *runLoopNode = head;
    while (runLoopNode != NULL)
    {
        cout << "|" << runLoopNode->maximumY;
        cout << "|" << runLoopNode->minimunX;
        cout << "|" << runLoopNode->inverseM;
        cout << "|" << runLoopNode->intensity;
        cout << "|" << runLoopNode->inverseIntensity<< "| -> ";
        
        runLoopNode = runLoopNode->nextNode;
    }
    cout << "\n";
}

void ET_LinkedList::updateX()								//update fnc, using for update a minimum of x by adding minimum of x with
{															//  inverse of m
    ET_Node *runLoopNode = head;
    
    while (runLoopNode != NULL)
    {
        runLoopNode->minimunX += runLoopNode->inverseM;
        runLoopNode->intensity =  runLoopNode->intensity + runLoopNode->inverseIntensity;
        runLoopNode->zMin = runLoopNode->zMin + runLoopNode->inverseZ;
        runLoopNode->color = runLoopNode->color + runLoopNode->inverseColor;
        
        runLoopNode = runLoopNode->nextNode;
        
    }
}

void ET_LinkedList::removeAtY(int y)						//remove fnc, removing nodes that have a maximum of y as y in parameter.
{
    if (head == NULL)
        return;
    
    ET_Node *runLoopNode = head;
    
    while (runLoopNode != NULL)
    {
        if (runLoopNode->nextNode != NULL)
        {
            if (runLoopNode->maximumY == y)
            {
                head = runLoopNode->nextNode;
                runLoopNode = runLoopNode->nextNode;
                length--;
            }
            else
            {
                break;
            }
        }
        else
        {
            if (runLoopNode->maximumY == y)
            {
                head = NULL;
                length = 0;
            }
            return;
        }
    }
    
    ET_Node *temp = runLoopNode;
    runLoopNode = runLoopNode->nextNode;
    
    while (runLoopNode->nextNode != NULL)
    {
        if (runLoopNode->maximumY == y)
        {
            temp->nextNode = runLoopNode->nextNode;
            runLoopNode = runLoopNode->nextNode;
            length--;
            continue;
        }
        
        runLoopNode = runLoopNode->nextNode;
        temp = temp->nextNode;
    }
    
    if (runLoopNode->maximumY == y)
    {
        temp->nextNode = NULL;
        length--;
    }
    
}

ET_LinkedList ET_LinkedList::sort()						//sort fnc, I use the add fnc to sort nodes.
{														//I set a new linkedlist and add nodes to it.
    ET_LinkedList result = ET_LinkedList();				//when it's done, I return that for this fnc.
    ET_Node *runLoopNode = head;
    while (runLoopNode != NULL)
    {
        result.add(runLoopNode->maximumY, runLoopNode->minimunX, runLoopNode->inverseM, runLoopNode->zMin, runLoopNode->inverseZ, runLoopNode->intensity, runLoopNode->inverseIntensity, runLoopNode->color, runLoopNode->inverseColor);
        
        runLoopNode = runLoopNode->nextNode;
    }
    
    return result;
}

ET_Node ET_LinkedList::getNode(int index)			//get node fnc, using for return a node which is a right index.
{
    
    ET_Node *runLoopNode = head;
    
    for (int i = 0; i < index; i++)
    {
        runLoopNode = runLoopNode->nextNode;
    }
    
    ET_Node result;
    result.inverseM = runLoopNode->inverseM;
    result.maximumY = runLoopNode->maximumY;
    result.minimunX = runLoopNode->minimunX;
    result.zMin = runLoopNode->zMin;
    result.inverseZ = runLoopNode->inverseZ;
    result.intensity = runLoopNode->intensity;
    result.color = runLoopNode->color;
    result.inverseColor = runLoopNode->inverseColor;
    result.inverseIntensity = runLoopNode->inverseIntensity;
    return result;
}

int ET_LinkedList::getLength()					//using for returing a length of the linkedlist
{
    return length;
}
