//
//  MyLinkedList.hpp
//  openGLProject
//
//  Created by Panupong Kukutapan on 4/18/2560 BE.
//  Copyright © 2560 Panupong Kukutapan. All rights reserved.
//

#ifndef MyLinkedList_hpp
#define MyLinkedList_hpp

#include "Affine.h"

#include <iostream>

using namespace std;





struct ET_Node									
{
    int maximumY;
    float minimunX;
    float inverseM;
    
    float zMin;
    float inverseZ;
    
    float intensity;
    float inverseIntensity;
    
    Hcoords color;
    Hcoords inverseColor;
    
    ET_Node *nextNode;
};

class ET_LinkedList
{
private:
    ET_Node *head;
    int length;
    
public:
    ET_LinkedList(void);
    void add(int yMax, float xMin, float invM, float zmin, float inversez, float I, float inverseI, Hcoords c, Hcoords inverseC);
    void updateX();
    void removeAtY(int y);
    void print();
    int getLength();
    ET_Node getNode(int index);
    ET_LinkedList sort();
    
};

ostream& operator<<(ostream& s, const Hcoords& v);

#endif /* MyLinkedList_hpp */
