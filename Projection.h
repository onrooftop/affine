//
//  Projection.h
//  openGLProject
//
//  Created by Panupong Kukutapan on 4/20/2560 BE.
//  Copyright © 2560 Panupong Kukutapan. All rights reserved.
//

#ifndef Projection_h
#define Projection_h


#define _CRT_SECURE_NO_DEPRECATE


#include <math.h>
#include <GLUT/GLUT.h>
#include <iostream>
#include <stdlib.h>
#include <cmath>
#include <stdio.h>
#include <vector>
#include "Affine.h"

using namespace std;

const float vpXmin = 0;
const float vpXmax = 600;
const float vpYmin = 0;
const float vpYmax = 600;
const float vpZmin = 0;
const float vpZmax = 200;

int const PERSPECTIVE = 0;
int const PARALLEL = 1;


bool readOBJ(const char* path, vector<Hcoords> &out_vertices, vector<Hcoords> &normals, vector<Hcoords> &color);



void parProjection();
void perProjection();
void init();

bool readOBJFile(const char* path, vector<Hcoords> &out_vertices);



void initProjection(int const typeOfProjection);

vector<Hcoords> project(const char* path);
vector<Hcoords> project(vector<Hcoords> &vertices);






#endif /* Projection_h */
