//
//  PolygonFilling.h
//  openGLProject
//
//  Created by Panupong Kukutapan on 4/18/2560 BE.
//  Copyright © 2560 Panupong Kukutapan. All rights reserved.
//

#ifndef PolygonFilling_h
#define PolygonFilling_h

#include "MyLinkedList.hpp"
#include "Affine.h"


#include <iostream>
#include <GLUT/GLUT.h>
#include <vector>
#include <math.h>

using namespace std;

int const WINDOW_HEIGHT = 600;
int const WINDOW_WIDTH = 600;
int const WIDTH = 600;
int const HEIGHT = 600;
int const MAX_HEIGHT_OF_SCANLINE = 600;



void initZbuffer(void);
bool checkZbuffer(int x, int y, float z);

class PolygonFilling{
    
private:
    vector<Hcoords> polygonPoints;
    vector<Hcoords> polygonColors;
    vector<float>   intensitys;
    
    ET_LinkedList *ET[MAX_HEIGHT_OF_SCANLINE];
    
    int counter = 0;
    int counterDefault = 0;
    
    bool isDrawingLinePolygon;
    bool isFillingColor;
    bool isDrawingPointMarker;
    
    
    
    void drawPointMarker(void);
    void drawLinePolygon(void);
    void clear(void);
    void drawPointsWithRange(int start, int end, int y, float zLeft, float zRight, float intensityLeft, float intensityRight, Hcoords colorLeft, Hcoords colorRight);
    void setupET(void);
    void fillColor(void);
    void init(void);
    
public:
    
    PolygonFilling(vector<Hcoords> pps, vector<float> is, vector<Hcoords> pcs);
    


    void display(void);
    
    
};


#endif /* PolygonFilling_h */
