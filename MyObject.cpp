//
//  MyObject.cpp
//  openGLProject
//
//  Created by Panupong Kukutapan on 4/20/2560 BE.
//  Copyright © 2560 Panupong Kukutapan. All rights reserved.
//

#include "MyObject.hpp"




void Object::calIntensity()
{
    Hcoords lightPosition(22,4,60,1);
    Hcoords viewer(16,8,60,1);;
    float ambientIntensity = 0.5;
    float lightIntensity = 0.5;
    float Ka = 0.7;
    float Kdiff = 0.9;
    float Kspec = 0.6;
    int n = 10;
    
    
    for (int i = 0; i < out_vertices.size(); i++)
    {
        Hcoords p = out_vertices[i];
        Hcoords N;
        
        for (int j = 0; j < out_vertices.size() ; j += 3)
        {
            Hcoords p1 = out_vertices[j];
            Hcoords p2 = out_vertices[j+1];
            Hcoords p3 = out_vertices[j+2];
            
            if(p == p1 || p == p2 || p == p3)
            {
                N = N + normals[j];
            }
            
        }
        
        N.w = 0;
        
        N = N / magnitude(N);
        
        
        
        Hcoords L = Normalize(lightPosition - p);
        
        Hcoords R = 2 * N * dot(N,L) - L;
        Hcoords V = Normalize(viewer - p);
        
        
        float ambient = ambientIntensity * Ka;
        float diffuse = lightIntensity * Kdiff * dot(N,L);
        float specular = lightIntensity * Kspec * powf(dot(R,V),n);
        
        
        float intensity = ambient + diffuse + specular;
        
       
        intensitys.push_back(intensity);
        
    }
    
    
    
    
}

void Object::trigulatePoints()
{
    
    
    
    for (int i = 0; i <= points.size() - 3; i += 3) {
        vector<Hcoords> temp;
        vector<Hcoords> tempC;
        vector<float> tempI;
        
        
        temp.push_back(points[i]);
        temp.push_back(points[i+1]);
        temp.push_back(points[i+2]);
        
        
        tempC.push_back(colors[i]);
        tempC.push_back(colors[i+1]);
        tempC.push_back(colors[i+2]);
        
        tempI.push_back(intensitys[i]);
        tempI.push_back(intensitys[i+1]);
        tempI.push_back(intensitys[i+2]);
        
        
//        cout << i << endl;
        ps.push_back(new PolygonFilling(temp, tempI, tempC));
        
        tempC.clear();
        temp.clear();
    }
    
    
    
}

void Object::displayPolygons()
{
    for(PolygonFilling *p : ps)
    {
        p->display();
    }
}

void Object::initObject()
{
    calIntensity();
    points = project(out_vertices);
    
//    for(Hcoords p: points)
//    {
//        cout << p << endl;
//    }
    
    
    trigulatePoints();

}


Object::Object(const char* path)
{
    out_vertices.clear();
    normals.clear();
    colors.clear();
    
    
    readOBJ(path, out_vertices, normals, colors);
    initObject();

}



