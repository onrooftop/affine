//Panupong Kukutapan 5709680010
//Assignment 2
//Computer Graphics 2
//Semester 2/2016

#include "Affine.h"

Hcoords::Hcoords(void) {
	x = 0;
	y = 0;
	z = 0;
	w = 0;
}
Hcoords::Hcoords(float X, float Y, float Z, float W) {
	x = X;
	y = Y;
	z = Z;
	w = W;
}

Point::Point(void) {
	x = 0;
	y = 0;
	z = 0;
	w = 1;
}
Point::Point(float X, float Y, float Z) {
	x = X;
	y = Y;
	z = Z;
	w = 1;
}


Vector::Vector(void) {
	x = 0;
	y = 0;
	z = 0;
	w = 0;
}
Vector::Vector(float X, float Y, float Z) {
	x = X;
	y = Y;
	z = Z;
	w = 0;
}
bool Vector::Normalize(void) {
	if (x == 0 && y == 0)
		return false;
	return true;
}


Affine::Affine(void) {
	row[0] = Hcoords(0, 0, 0, 0);
	row[1] = Hcoords(0, 0, 0, 0);
	row[2] = Hcoords(0, 0, 0, 0);
	row[3] = Hcoords(0, 0, 0, 1);
}
Affine::Affine(const Hcoords& Lx, const Hcoords& Ly, const Hcoords& Lz, const Hcoords& disp) {
	row[0] = Hcoords(Lx.x, Ly.x, Lz.x, disp.x);
	row[1] = Hcoords(Lx.y, Ly.y, Lz.y, disp.y);
	row[2] = Hcoords(Lx.z, Ly.z, Lz.z, disp.z);
	row[3] = Hcoords(Lx.w, Ly.w, Lz.w, disp.w);
}

Hcoords operator+(const Hcoords& u, const Hcoords& v) {
	float x, y, z, w;
	x = u.x + v.x;
	y = u.y + v.y;
	z = u.z + v.z;
	w = u.w + v.w;
	return Hcoords(x, y, z, w);
}


Hcoords operator-(const Hcoords& u, const Hcoords& v) {
	float x, y, z, w;
	x = u.x - v.x;
	y = u.y - v.y;
	z = u.z - v.z;
	w = u.w - v.w;
	return Hcoords(x, y, z, w);
}

Hcoords operator/(const Hcoords& u, const float v)
{
    float x, y, z, w;
    x = u.x / v;
    y = u.y / v;
    z = u.z / v;
    w = u.w / v;
    
    return Hcoords(x,y,z,w);
    
}

Hcoords operator*(const Hcoords& u, const float v)
{
    float x, y, z, w;
    x = u.x * v;
    y = u.y * v;
    z = u.z * v;
    w = u.w * v;
    
    return Hcoords(x,y,z,w);
    
}


Hcoords operator-(const Hcoords& v) {
	float x, y, z, w;
	x = v.x*-1;
	y = v.y*-1;
	z = v.z*-1;
	w = v.w*-1;
	return Hcoords(x, y, z, w);
}


Hcoords operator*(float r, const Hcoords& v) {
	float x, y, z, w;
	x = v.x*r;
	y = v.y*r;
	z = v.z*r;
	w = v.w*r;
	return Hcoords(x, y, z, w);
}


Hcoords operator*(const Affine& A, const Hcoords& v) {
	int x = 0;
	int y = 1;
	int z = 2;
	int w = 3;
	float _hcoords[4];

	for (int i = 0; i < 4; i++) {
		Hcoords h = A.operator[](i);
		_hcoords[i] = h.x * v.x + h.y * v.y + h.z * v.z + h.w * v.w;
	}
	return Hcoords(_hcoords[x], _hcoords[y], _hcoords[z], _hcoords[w]);
}


Affine operator*(const Affine& A, const Affine& B) {
	Hcoords columns[4];

	for (int i = 0; i < 4; i++) {

		columns[i].x = A.operator[](0).x * B.operator[](0).operator[](i) +
			A.operator[](0).y * B.operator[](1).operator[](i) +
			A.operator[](0).z * B.operator[](2).operator[](i) +
			A.operator[](0).w * B.operator[](3).operator[](i);

		columns[i].y = A.operator[](1).x * B.operator[](0).operator[](i) +
			A.operator[](1).y * B.operator[](1).operator[](i) +
			A.operator[](1).z * B.operator[](2).operator[](i) +
			A.operator[](1).w * B.operator[](3).operator[](i);

		columns[i].z = A.operator[](2).x * B.operator[](0).operator[](i) +
			A.operator[](2).y * B.operator[](1).operator[](i) +
			A.operator[](2).z * B.operator[](2).operator[](i) +
			A.operator[](2).w * B.operator[](3).operator[](i);

		columns[i].w = A.operator[](3).x * B.operator[](0).operator[](i) +
			A.operator[](3).y * B.operator[](1).operator[](i) +
			A.operator[](3).z * B.operator[](2).operator[](i) +
			A.operator[](3).w * B.operator[](3).operator[](i);
	}

	return Affine(columns[0], columns[1], columns[2], columns[3]);

}


float dot(const Vector& u, const Vector& v) {
	return u.x*v.x + u.y*v.y + u.z*v.z;
}

Vector cross(const Vector& a, const Vector &b)
{
	return Vector(a.y * b.z - a.z * b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x);
}


float abs(const Vector& v) {
	return sqrtf(powf(v.x, 2) + powf(v.y, 2) + powf(v.z,2));
}


Hcoords Normalize(const Hcoords& u)
{
	Hcoords result;
	float dd = sqrtf(powf(u.x, 2) + powf(u.y, 2) + powf(u.z, 2) + powf(u.w,2));
	result.x = u.x / dd;
	result.y = u.y / dd;
	result.z = u.z / dd;
	result.w = u.w / dd;

	return result;
}


Affine Trans(const Vector& v) {

	Hcoords columns[4];
	columns[0] = Hcoords(1, 0, 0, 0);
	columns[1] = Hcoords(0, 1, 0, 0);
	columns[2] = Hcoords(0, 0, 1, 0);
	columns[3] = Hcoords(v.x, v.y, v.z, 1);

	return Affine(columns[0], columns[1], columns[2], columns[3]);

}

Affine Scale(float r) {

	Hcoords columns[4];
	columns[0] = Hcoords(r, 0, 0, 0);
	columns[1] = Hcoords(0, r, 0, 0);
	columns[2] = Hcoords(0, 0, r, 0);
	columns[2] = Hcoords(0, 0, 0, 1);

	return Affine(columns[0], columns[1], columns[2], columns[3]);
}


Affine Scale(float rx, float ry, float rz) {

	Hcoords columns[4];
	columns[0] = Hcoords(rx, 0, 0, 0);
	columns[1] = Hcoords(0, ry, 0, 0);
	columns[2] = Hcoords(0,  0, rz, 0);
	columns[3] = Hcoords(0, 0, 0, 1);

	return Affine(columns[0], columns[1], columns[2], columns[3]);

}

Affine Shear(float shx, float shy)
{
	Hcoords columns[4];
	columns[0] = Hcoords(1, 0, 0, 0);
	columns[1] = Hcoords(0, 1, 0, 0);
	columns[2] = Hcoords(shx, shy, 1, 0);
	columns[3] = Hcoords(0, 0, 0, 1);

	return Affine(columns[0], columns[1], columns[2], columns[3]);
}

Affine MappingIntoViewport(float Zmin)
{
	Hcoords columns[4];
	columns[0] = Hcoords(1, 0, 0, 0);
	columns[1] = Hcoords(0, 1, 0, 0);
	columns[2] = Hcoords(0, 0, 1/(1+Zmin), -1);
	columns[3] = Hcoords(0, 0, -Zmin / (1 + Zmin), 0);

	return Affine(columns[0], columns[1], columns[2], columns[3]);
}


Affine CamToWorld(const Vector& x, const Vector& y, const Vector& z)
{
	Point dsp;
	Vector c1(x.x, y.x, z.x);
	Vector c2(x.y, y.y, z.y);
	Vector c3(x.z, y.z, z.z);


	return Affine(c1, c2, c3, dsp);
}


Hcoords DivideByW(const Hcoords& u)
{
	Hcoords result;
	float w = u.w;
	result.x = u.x / w;
	result.y = u.y / w;
	result.z = u.z / w;
	result.w = u.w / w;

	return result;
}

float magnitude(const Hcoords &u)
{
    return sqrtf(u.x * u.x + u.y * u.y + u.z * u.z);
}



bool operator==(const Hcoords& u, const Hcoords& v)
{
    return u.x == v.x && u.y == v.y && u.z == v.z;
}

