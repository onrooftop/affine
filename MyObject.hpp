//
//  MyObject.hpp
//  openGLProject
//
//  Created by Panupong Kukutapan on 4/20/2560 BE.
//  Copyright © 2560 Panupong Kukutapan. All rights reserved.
//

#ifndef MyObject_hpp
#define MyObject_hpp

#include <vector>

#include "Affine.h"
#include "PolygonFilling.h"
#include "Projection.h"

using namespace std;


class Object{
private:
    void calIntensity();
    void trigulatePoints();
    void initObject();
public:
    vector<PolygonFilling *> ps;
    vector<float> intensitys;
    std::vector<Hcoords> out_vertices;
    std::vector<Hcoords> points;
    std::vector<Hcoords> normals;
    std::vector<Hcoords> colors;
    
    Object(const char* path);
    void displayPolygons();
};


#endif /* MyObject_hpp */
