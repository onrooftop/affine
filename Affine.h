
#ifndef CS200_AFFINE_H
#define CS200_AFFINE_H

#include <cmath>
#include <cassert>


struct Hcoords {
	float x, y, z, w;
	Hcoords(void);
	Hcoords(float X, float Y, float Z, float W);
	float& operator[](int i) { return *(&x + i); }
	float operator[](int i) const { return *(&x + i); }
	static bool Near(float x, float z) { return std::abs(x - z) < 1e-5f; }
};


struct Point : Hcoords {
	Point(void);
	Point(float X, float Y, float Z);
	Point(const Hcoords& v) : Hcoords(v) { assert(Near(w, 1)); }
};


struct Vector : Hcoords {
	Vector(void);
	Vector(float X, float Y, float Z);
	Vector(const Hcoords& v) : Hcoords(v) { assert(Near(w, 0)); }
	bool Normalize(void);
};


struct Affine {
	Hcoords row[4];
	Affine(void);
	Affine(const Hcoords& Lx, const Hcoords& Ly, const Hcoords& Lz, const Hcoords& disp);
	Hcoords& operator[](int i) { return row[i]; }
	const Hcoords& operator[](int i) const { return row[i]; }
};


Hcoords operator+(const Hcoords& u, const Hcoords& v);
Hcoords operator-(const Hcoords& u, const Hcoords& v);

Hcoords operator/(const Hcoords& u, const float v);
Hcoords operator*(const Hcoords& u, const float v);

bool operator==(const Hcoords& u, const Hcoords& v);

Hcoords operator-(const Hcoords& v);
Hcoords operator*(float r, const Hcoords& v);
Hcoords operator*(const Affine& A, const Hcoords& v);
Affine operator*(const Affine& A, const Affine& B);

Hcoords Normalize(const Hcoords& u);
Hcoords DivideByW(const Hcoords& u);


float magnitude(const Hcoords &u);
float dot(const Vector& u, const Vector& v);
float abs(const Vector& v);

//Affine WorldToCam(const Vector& x, const Vector& y, const Vector& z);
Affine CamToWorld(const Vector& x, const Vector& y, const Vector& z);

Affine Trans(const Vector& v);
Affine Scale(float r);
Affine Shear(float shx, float shy);
Affine Scale(float rx, float ry, float rz);
Affine Inverse(const Affine& A);
Affine MappingIntoViewport(float Zmin);

Vector cross(const Vector& a, const Vector &b);


#endif
