//
//  main.cpp
//  openGLProject
//
//  Created by Panupong Kukutapan on 4/18/2560 BE.
//  Copyright © 2560 Panupong Kukutapan. All rights reserved.
//

#include "MyObject.hpp"


Object *a;
Object *b;
Object *c;


void createObject();

void init()
{
    
    glClearColor(1.0f,1.0f,1.0f,1.0f);
    glMatrixMode(GL_PROJECTION);
    
    initProjection(PERSPECTIVE);
    initZbuffer();
    
    createObject();

}

void createObject()
{
    //a = new Object("/Users/panupong/Desktop/openGLProject/openGLProject/House.txt");
    b = new Object("/Users/panupong/Desktop/openGLProject/openGLProject/tri2.txt");
    c = new Object("/Users/panupong/Desktop/openGLProject/openGLProject/tri3.txt");
}




void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);
    
    
    //a->displayPolygons();
    c->displayPolygons();
    b->displayPolygons();


    
    glutSwapBuffers();
}



int main(int argc, char *argv[])
{
    
    
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(600,600);
    glutInitWindowPosition(300, 300);
    glutCreateWindow("Polygon Filling");
    init();
    glutDisplayFunc(display);
    glutMainLoop();
    
    return 0;
}
